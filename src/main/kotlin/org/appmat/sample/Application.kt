package org.appmat.sample

import io.ktor.application.*
import io.ktor.server.netty.*
import org.appmat.sample.greeting.greetingModule
import org.appmat.sample.plugins.monitoring.configureMonitoring
import org.appmat.sample.plugins.di.configureDI
import org.appmat.sample.plugins.healthcheck.configureHealthChecks
import org.appmat.sample.plugins.routing.configureRouting
import org.appmat.sample.plugins.serialization.configureSerialization
import org.koin.core.module.Module

fun main(args: Array<String>): Unit = EngineMain.main(args)

/**
 * Please note that you can use any other name instead of *module*.
 * Also note that you can have more then one modules in your application.
 * */
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false, koinModules: List<Module> = listOf(greetingModule)) {
    configureDI(koinModules)
    configureRouting()
    configureMonitoring()
    configureSerialization()
    configureHealthChecks()
}
