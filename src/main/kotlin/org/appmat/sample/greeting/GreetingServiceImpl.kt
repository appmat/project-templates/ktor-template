package org.appmat.sample.greeting

class GreetingServiceImpl : GreetingService {
    override fun greet() = "Hello World!"
}
