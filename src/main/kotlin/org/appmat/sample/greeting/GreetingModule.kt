package org.appmat.sample.greeting

import org.appmat.sample.plugins.routing.KtorController
import org.koin.dsl.module

val greetingModule = module {
    single<GreetingService> { GreetingServiceImpl() }
    single<KtorController> { GreetingController(get()) }
}
