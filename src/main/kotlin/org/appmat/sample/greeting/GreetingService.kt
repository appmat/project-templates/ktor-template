package org.appmat.sample.greeting

interface GreetingService {
    fun greet(): String
}

