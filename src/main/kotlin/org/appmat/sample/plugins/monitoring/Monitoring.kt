package org.appmat.sample.plugins.monitoring

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.locations.*
import io.ktor.metrics.micrometer.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import org.appmat.plugins.Metrics
import org.slf4j.event.Level

fun Application.configureMonitoring() {
    install(CallLogging) {
        level = Level.INFO
        val methodsToIgnore = Regex("^/(metrics|healthz|readyz)")
        filter { call ->
            call.request.path().matches(methodsToIgnore).not()
        }
    }
    val appMicrometerRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

    install(MicrometerMetrics) {
        registry = appMicrometerRegistry
    }

    routing {
        get<Metrics> {
            call.respond(appMicrometerRegistry.scrape())
        }
    }
}
