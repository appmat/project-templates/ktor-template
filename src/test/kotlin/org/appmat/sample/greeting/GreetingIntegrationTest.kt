package org.appmat.sample.greeting

import io.kotest.assertions.ktor.shouldHaveContent
import io.kotest.assertions.ktor.shouldHaveStatus
import io.kotest.core.spec.style.StringSpec
import io.ktor.http.*
import io.ktor.server.testing.*
import org.appmat.sample.withAppIntegrationTest

class GreetingIntegrationTest : StringSpec({
    "hello endpoint returns greeting" {
        withAppIntegrationTest {
            handleRequest(HttpMethod.Get, "/greeting").apply {
                response shouldHaveStatus HttpStatusCode.OK
                response shouldHaveContent "Hello World!"
            }
        }

    }
})
