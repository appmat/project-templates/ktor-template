package org.appmat.sample

import io.ktor.server.testing.*

fun <R> withAppIntegrationTest(test: TestApplicationEngine.() -> R) =
    withApplication(createTestEnvironment {
        module {
            module(testing = true)
        }
    }) {
        test()
    }
