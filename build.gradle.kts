import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val koin_version: String by project
val ktor_version: String by project
val kotest_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val prometeus_version: String by project

plugins {
    application
    kotlin("jvm") version "1.5.20"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.5.20"
}

group = "org.appmat"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.netty.EngineMain")
}

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    // Ktor
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-serialization:$ktor_version")
    implementation("io.ktor:ktor-metrics-micrometer:$ktor_version")
    implementation("io.micrometer:micrometer-registry-prometheus:$prometeus_version")
    implementation("io.ktor:ktor-metrics:$ktor_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")

    // Health check
    implementation("com.github.zensum:ktor-health-check:011a5a8")

    // Koin
    implementation("io.insert-koin:koin-ktor:$koin_version")
    implementation("io.insert-koin:koin-logger-slf4j:$koin_version")
    testImplementation("io.insert-koin:koin-test-junit5:$koin_version")

    // Kotest
    testImplementation("io.kotest:kotest-runner-junit5:$kotest_version")
    testImplementation("io.kotest:kotest-assertions-core:$kotest_version")
    testImplementation("io.kotest:kotest-property:$kotest_version")
    testImplementation("io.kotest:kotest-property-datetime:$kotest_version")
    testImplementation("io.kotest.extensions:kotest-extensions-koin:1.0.0")
    testImplementation("io.kotest.extensions:kotest-assertions-ktor:1.0.3")
}

/**
 * stage task is executed in buildpack jvm pipeline
 * @see <a href="https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku">https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku</a>
 */
tasks.register("stage") {
    dependsOn("installDist")
    /**
     * generates the Procfile that specifies a command to run the application
     */
    doLast {
        val relativeInstallationPath = tasks.installDist.get().destinationDir.relativeTo(project.projectDir)
        File("Procfile").writeText(
            """
                web: ./$relativeInstallationPath/bin/${project.name}
            """.trimIndent()
        )
    }
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.freeCompilerArgs += "-Xopt-in=io.ktor.locations.KtorExperimentalLocationsAPI"
}
